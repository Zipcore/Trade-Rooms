This plugin creates voice zones for trade servers.
Player can only talk to each other when they are inside the same zone or not inside a zone.
Additionally announces join/leave events to all players which are already isnide the zone.

~haVe fuN~

Installation:
=============
* 1.) Upload the .SMX file to sourcemod/plugins
* 2.) Add "traderooms" to your databases.cfg
* 3.) restart the server


Commands:
=============
* vz_add (roomnumber)
* vz_delete
* vz_reload