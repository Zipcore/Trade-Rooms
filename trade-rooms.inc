#if defined _trade_rooms_included
  #endinput
#endif
#define _trade_rooms_included

/* Forwards */

forward void TradeRooms_EnterRoom(int client, int room);
forward void TradeRooms_LeaveRoom(int client, int room);

/* Natives */

native int TradeRooms_GetRoom(int client);

public void __pl_trade_rooms_SetNTVOptional() 
{
	MarkNativeAsOptional("TradeRooms_GetRoom");
}